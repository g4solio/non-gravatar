
OUT='a.out'
if [ $# -ne 0 ]
then
	OUT=$1
fi
g++ -o $OUT $(find . -type d -name "Test" -prune -o -type f -iname *.cpp -print) -g -lsfml-graphics -lsfml-window -lsfml-system

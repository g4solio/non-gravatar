#ifndef MVC_LIBRARIES_H
#define MVC_LIBRARIES_H

#include "Libraries.hpp"
#include "../Controllers/Controller.hpp"
#include "../Models/Model.hpp"
#include "../Views/View.hpp"
#include "MVC_Master.hpp"

#endif
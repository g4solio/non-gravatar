#ifndef ARGUMENTS_H
#define ARGUMENTS_H


#include "Libraries.hpp"

struct Arguments 
{
    std::list <float> arguments_float;
    std::list <std::string> arguments_string;
    std::list <char> arguments_char;

};

#endif
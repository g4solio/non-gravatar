#ifndef MVC_MASTER_H
#define MVC_MASTER_H

#include "Libraries.hpp"
#include "../Controllers/Controller.hpp"
#include "../Views/View.hpp"
using namespace std;

class Controller;
class View;
class Model;

class MVC_Master
{
    private:
        
        list <Controller*> controllers;
        list <View*> views;
    
        static MVC_Master* instance;
    public:

        
        //std::vector<Model*> modelsChildrens;

        //Model* GetModelObject(const char* typeClass);

        MVC_Master(/* args */);
        void AddController(Controller *controller);
        void AddView(View *view);
        void NotifyEvent(string event, Controller *caller, Arguments* arguments, bool notifyView = true);
        static MVC_Master* getInstance();


};

// template<class T>
// struct insert {
//     T* instance;
//     insert() {MVC_Master::getInstance()->modelsChildrens.push_back(instance);}
// };



#endif

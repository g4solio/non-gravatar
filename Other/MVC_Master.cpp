#include "MVC_Master.hpp"

MVC_Master* MVC_Master::instance = 0;


void MVC_Master::AddController(Controller *controller)
{
    controllers.push_front(controller);
};

void MVC_Master::AddView(View *view)
{
    views.push_front(view);
};



void MVC_Master::NotifyEvent(string event, Controller *caller, Arguments* arguments, bool notifyView)
{
    for(auto i = controllers.begin(); i != controllers.end(); ++i)
    {
        (*i)->GetNotify(event, caller, arguments);
    }           
    if(!notifyView) return;
    for(auto i = views.begin(); i!= views.end(); ++i)
    {
        (*i)->GetNotify(event, caller, arguments);
    }
}

MVC_Master::MVC_Master()
{

}

MVC_Master* MVC_Master::getInstance()
{
    if (instance == 0)
    {
        instance = new MVC_Master();
    }

    return instance;
}


// Model* MVC_Master::GetModelObject(const char* typeClass)
// {
//     for(auto i = modelsChildrens.begin(); i != modelsChildrens.end(); ++i)
//     {

//         if(typeid(*i).name() == typeClass) return *i;

//     }
//     return NULL;
// }



#ifndef VIEW_H
#define VIEW_H

#include "../Other/Libraries.hpp"
#include "../Other/MVC_Master.hpp"

using namespace std;

class Controller;
class MVC_Master;

class View
{
private:
    MVC_Master *master;
public:

    View();
    virtual void GetNotify(string event, Controller *caller, Arguments* arguments);
    
};


#endif
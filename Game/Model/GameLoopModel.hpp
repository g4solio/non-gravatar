#ifndef GAME_LOOP_MODEL_H
#define GAME_LOOP_MODEL_H


#include "../../Other/MVC_Libraries.hpp"
#include <SFML/Graphics.hpp>

class GameLoopModel : public Model
{

private:
    static GameLoopModel* instance;

public:
    GameLoopModel();
    virtual ~GameLoopModel(){};
    static GameLoopModel* GetInstance();


    sf::RenderWindow* window;

};




#endif
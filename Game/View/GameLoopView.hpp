#ifndef GAME_LOOP_VIEW_H
#define GAME_LOOP_VIEW_H


#include "../../Other/MVC_Libraries.hpp"
#include <SFML/Graphics.hpp>
#include "../Model/GameLoopModel.hpp"



class GameLoopView : public View
{
private:
    sf::RenderWindow *window;
public:
    GameLoopView(){};
    void GetNotify(string event, Controller *caller, Arguments* arguments);

};




#endif
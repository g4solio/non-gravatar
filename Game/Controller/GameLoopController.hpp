#ifndef GAME_LOOP_CONTROLLER_H
#define GAME_LOOP_CONTROLLER_H

#include "../../Other/MVC_Libraries.hpp"
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "../View/GameLoopView.hpp"

class GameLoopController : public Controller
{
private:

public:

    double frameRate = 8.33; //milliseconds
    double physicsRefreshRate = frameRate / 2;
    GameLoopController(){};
    void PhysicsLoop();
    void VisualLoop();
    void Loop();
    void GetNotify(string event, Controller *caller, Arguments* arguments);

};



#endif
#include "GameLoopController.hpp"




void GameLoopController::VisualLoop()
{
    double sleep = GameLoopController::frameRate;
    sf::RenderWindow window(sf::VideoMode(1000, 900), "NON GRAVATAR!!", sf::Style::Default);

    GameLoopModel *gLM = GameLoopModel::GetInstance();
    if(gLM != NULL) 
    {
        gLM->window = &window;
    }

    while (true)
    {

        while (window.isOpen())
        {
            
   

            // check all the window's events that were triggered since the last iteration of the loop
            sf::Event event;
            while (window.pollEvent(event))
            {
                // "close requested" event: we close the window
                if (event.type == sf::Event::Closed)
                {
                    window.close();
                    return;
                }
            }
            window.clear();
            Notify("VisualLoop_clear", NULL);
            
            // std::cout << "Visual Loop" << '\n';
            //Notify("VisualLoop", NULL);
            Notify("VisualLoop_show", NULL);
            window.display();

            sf::sleep(sf::milliseconds(sleep));        

        }
    }

}

void GameLoopController::PhysicsLoop()
{
    double sleep = GameLoopController::physicsRefreshRate;
    while (true)
    {

        //std::cout << "Physic Loop" << '\n';
        Notify("GameLoop_Await", NULL);
        Notify("GameLoop_Update", NULL);        

        sf::sleep(sf::milliseconds(sleep));
    }

}

void GameLoopController::Loop()
{

    GameLoopView();
    GameLoopModel();
    sf::Thread visualThread(&GameLoopController::VisualLoop, this);
    sf::Thread physicThread(&GameLoopController::PhysicsLoop, this);


    visualThread.launch();
    physicThread.launch();

    visualThread.wait();
    physicThread.terminate();
}


void GameLoopController::GetNotify(string event, Controller *caller, Arguments* arguments)
{

    //cout<< event << endl;


}


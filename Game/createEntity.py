#!/usr/bin/python
import sys
import io




controller = sys.argv[1] + "Controller"

model = sys.argv[1] + "Model"

view = sys.argv[1] + "View"



_controller = sys.argv[1] + "_Controller"

_model = sys.argv[1] + "_Model"

_view = sys.argv[1] + "_View"


templates = {"ControllerTemplate" : "Controller/"+ controller, "ModelTemplate" : "Model/" + model , "ViewTemplate": "View/" + view}



entity_type = {"controller" : [controller, _controller], "model" : [model, _model], "view" : [view, _view]}


def replace_line(line):
    for type in entity_type.keys():
        line = line.replace("$object_" + type + "_header", (entity_type[type][1] + "_hpp").upper())
        line = line.replace("$object_" + type + "_hpp", entity_type[type][0] + ".hpp")
        line = line.replace("$object_" + type, entity_type[type][0])
    return line



for template in templates.keys():
    # 0 = hpp, 1 = cpp
    i = 0;
    while i < 2:
        out = io.open(templates[template] + (".hpp" if i == 0 else ".cpp"), "w")
        for line in io.open("Template/" + template + (".hpp" if i == 0 else ".cpp") + ".template", "r"):
            line = replace_line(line)
            print ("bulding file " + template)
            out.write(line)
        out.close()
        print(".hpp" if i == 0 else ".cpp" )
        i = i + 1












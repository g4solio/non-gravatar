
#include "Controller/GameLoopController.hpp"
#include "View/GameLoopView.hpp"


int main(int argc, char const *argv[])
{

    GameLoopController game = GameLoopController();
    game.Loop();

    return 0;
}




OUT='a.out'
if [ $# -ne 0 ]
then
	OUT=$1
fi
g++ -o $OUT $(find . -type f -iname *.cpp -print) -lsfml-graphics -lsfml-window -lsfml-system
